package facci.pm.meratorres.coraimaapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class UserDetailsActivity extends AppCompatActivity {

    TextView name, username,email,phone, website ;
    UserResponse userResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        name  = findViewById(R.id.name);
        username  = findViewById(R.id.username);
        email  = findViewById(R.id.email);
        phone  = findViewById(R.id.phone);
        website  = findViewById(R.id.website);

        Intent intent = getIntent();
        if(intent.getExtras() !=null){
            userResponse = (UserResponse) intent.getSerializableExtra("data");

            String namedata = userResponse.getName();
            String usernamedata = userResponse.getUsername();
            String useremail = userResponse.getEmail();
            String phonedata = userResponse.getPhone();

            String websitedata = userResponse.getWebsite();


            name.setText(namedata);
            username.setText(usernamedata);
            email.setText(useremail);
            phone.setText(phonedata);
            website.setText(websitedata);


        }


    }
}
